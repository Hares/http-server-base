from typing import Dict, Callable

from http_server_base import Logged_RequestHandler

class Arithmetic_RequestHandler(Logged_RequestHandler):
    
    logger_name = 'example_server.arithmetic_handler'
    actions: Dict[str, Callable] = None
    
    def initialize(self, **kwargs):
        super().initialize(**kwargs)
        self.actions = \
        {
            "add": self._add,
            "sub": self._sub,
            "mul": self._mul,
            "div": self._div,
        }
    
    def post(self, *args, **kwargs):
        _action = self.get_body_or_query_argument('action').lower()
        _args = self.get_body_or_query_arguments('arg')
        
        if (not _action in self.actions):
            self.resp_error(400, f"The action not allowed: {_action}")
            return
        try:
            _result = self.actions[_action](*[float(_arg) for _arg in _args])
        except TypeError as _ex:
            self.resp_error(400, f"Wrong arguments: {str(_ex)}")
        else:
            self.resp_success(message=f"Success. Result is {_result}", result=_result)
    
    def _add(self, a, b):
        return a + b
    def _sub(self, a, b):
        return a - b
    def _mul(self, a, b):
        return a * b
    def _div(self, a, b):
        return a / b


class Dynamic_RequestHandler(Logged_RequestHandler):
    logger_name = 'example_server.dynamic_handler'
    
    address: str
    source_name: str
    source_type: str
    def initialize(self, address: str, source_name: str, source_type: str, **kwargs):
        self.address = address
        self.source_name = source_name
        self.source_type = source_type
    
    def get(self, *args, **kwargs):
        self.render(template_name='dynamic_page.html')
