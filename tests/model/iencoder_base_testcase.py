from functools import partialmethod
from typing import Type, TypeVar, Any, Dict, List, Tuple

from http_server_base.model import IEncoder, EncoderError
from tests.helpers import TestCaseWithPrettyTestName

T = TypeVar('T')
class IEncoderBaseTestCase(TestCaseWithPrettyTestName):
    encoder: IEncoder[T] = IEncoder
    encoder_name: str
    encode_single_params: List[Tuple[Type[T], T, Dict[str, Any], Dict[str, Any]]] = list()
    
    @classmethod
    def setUpClass(cls):
        enc_type = cls.encoder
        if (not isinstance(enc_type, type)):
            enc_type = type(enc_type)
        cls.encoder_name = enc_type.__name__
        
        # Define test parameters
        cls.encode_many_params = [ (tp, [ value], [ expected], *other) for tp, value, expected, *other in cls.encode_single_params ]
        _encode_many_2 = [ (List[tp], [ value ], [ expected ], *other) for tp, value, expected, *other in cls.encode_single_params ]
        cls.encode_smart_params = cls.encode_single_params + _encode_many_2
        
        def invert(lst):
            return [ (tp, expected, value, *other) for tp, value, expected, *other in lst ]
        
        cls.decode_single_params = invert(cls.encode_single_params)
        cls.decode_many_params = invert(cls.encode_many_params)
        cls.decode_smart_params = invert(cls.encode_smart_params)
        
        # Define check functions
        cls.encode_functions = dict()
        cls.decode_functions = dict()
        for func_name in [ 'single', 'many', 'smart' ]:
            cls.encode_functions[func_name] = getattr(cls.encoder, f'encode_{func_name}')
            cls.decode_functions[func_name] = getattr(cls.encoder, f'decode_{func_name}')
            
            for prefix in [ 'encode', 'decode' ]:
                for suffix in [ '', '_raises']:
                    base_func = getattr(cls, f'_check_{prefix}{suffix}')
                    func = partialmethod(base_func, func_name=func_name)
                    setattr(cls, f'check_{prefix}{suffix}_{func_name}', func)
                
                test_func = getattr(cls, f'test_{prefix}_{func_name}_ok')
                test_func.__doc__ = test_func.__doc__.format(self=cls)
    
    def _check_coding_decode(self, expected, value, *, func_name: str, model, **kwargs):
        decoded = self.decode_functions[func_name](model, value, **kwargs)
        print(f"Decoded: '{decoded}'")
        self.assertEqual(expected, decoded)
        return decoded
    def _check_coding_encode(self, expected, value, *, func_name: str, model, **kwargs):
        encoded = self.encode_functions[func_name](model, value, **kwargs)
        print(f"Encoded: '{encoded}'")
        self.assertEqual(expected, encoded)
        return encoded
    
    def _check_encode(self, model: Type[T], value: T, expected: Dict[str, Any], *, func_name: str, **kwargs):
        with self.subTest(f"Encode/decode {model} {value}", **kwargs):
            print(f"Value: '{value}'")
            encoded = self._check_coding_encode(expected, value, func_name=func_name, model=model, **kwargs)
            decoded = self._check_coding_decode(value, encoded, func_name=func_name, model=model, **kwargs)
            print()
    def check_encode_single(self, model: Type[T], value: T, expected: Dict[str, Any], **kwargs): pass
    def check_encode_many  (self, model: Type[T], value: T, expected: List[Dict[str, Any]], **kwargs): pass
    def check_encode_smart (self, model: Type[T], value: T, expected: Dict[str, Any], **kwargs): pass
    
    def _check_decode(self, model: Type[T], value: Dict[str, Any], expected: T, *, func_name: str, **kwargs):
        with self.subTest(f"Decode/encode {model} {value}", **kwargs):
            print(f"Value: '{value}'")
            decoded = self._check_coding_decode(expected, value, func_name=func_name, model=model, **kwargs)
            encoded = self._check_coding_encode(value, decoded, func_name=func_name, model=model, **kwargs)
            print()
    def check_encode_raises_single(self, model: Type[T], error: Type[Exception] = EncoderError, **kwargs): pass
    def check_encode_raises_many  (self, model: Type[T], error: Type[Exception] = EncoderError, **kwargs): pass
    def check_encode_raises_smart (self, model: Type[T], error: Type[Exception] = EncoderError, **kwargs): pass
    
    def _check_encode_raises(self, model: Type[T], value: T, error: Type[Exception] = EncoderError, *, func_name: str, **kwargs):
        with self.subTest(f"Error at encoding {model} {value}", **kwargs):
            print(f"Value: '{value}'")
            print()
            self.assertRaises(error, self.encode_functions[func_name], model, value, **kwargs)
    
    def check_decode_single(self, model: Type[T], value: Dict[str, Any], expected: T, **kwargs): pass
    def check_decode_many  (self, model: Type[T], value: List[Dict[str, Any]], expected: T, **kwargs): pass
    def check_decode_smart (self, model: Type[T], value: Dict[str, Any], expected: T, **kwargs): pass
    
    def _check_decode_raises(self, model: Type[T], value: Dict[str, Any], error: Type[Exception] = EncoderError, *, func_name: str, **kwargs):
        with self.subTest(f"Error at decoding {model} {value}", **kwargs):
            print(f"Encoder: {self.encoder}")
            print(f"Model: {model}")
            print(f"Value: '{value}'")
            print(f"Func: {self.decode_functions[func_name]}")
            print()
            self.assertRaises(error, self.decode_functions[func_name], model, value, **kwargs)
    
    def check_decode_raises_single(self, model, value, error: Type[Exception] = EncoderError, **kwargs): pass
    def check_decode_raises_many  (self, model, value, error: Type[Exception] = EncoderError, **kwargs): pass
    def check_decode_raises_smart (self, model, value, error: Type[Exception] = EncoderError, **kwargs): pass
    
    def test_encode_single_ok(self):
        """
        Test {self.encoder_name}.encode_single() method
        """
        for model, value, expected, kwargs in self.encode_single_params:
            self.check_encode_single(model, value, expected, **kwargs)
    def test_encode_many_ok(self):
        """
        Test {self.encoder_name}.encode_many() method
        """
        for model, value, expected, kwargs in self.encode_many_params:
            self.check_encode_many(model, value, expected, **kwargs)
    def test_encode_smart_ok(self):
        """
        Test {self.encoder_name}.encode_smart() method
        """
        for model, value, expected, kwargs in self.encode_smart_params:
            self.check_encode_smart(model, value, expected, **kwargs)
    
    def test_decode_single_ok(self):
        """
        Test {self.encoder_name}.decode_single() method
        """
        for model, value, expected, kwargs in self.decode_single_params:
            self.check_decode_single(model, value, expected, **kwargs)
    def test_decode_many_ok(self):
        """
        Test {self.encoder_name}.decode_many() method
        """
        for model, value, expected, kwargs in self.decode_many_params:
            self.check_decode_many(model, value, expected, **kwargs)
    def test_decode_smart_ok(self):
        """
        Test {self.encoder_name}.decode_smart() method
        """
        for model, value, expected, kwargs in self.decode_smart_params:
            self.check_decode_smart(model, value, expected, **kwargs)
