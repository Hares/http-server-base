from .iencoder_base_testcase import IEncoderBaseTestCase

SimpleClass, SimpleClassWithInit, Dataclass, InheritedJsonDataclass, DecoratedJsonDataclass, JsonDataClassWithOptionals = type, type, type, type, type, type
simple_class_instance, simple_class_with_init_instance, dataclass_instance, inherited_json_dataclass_instance, decorated_json_dataclass_instance, empty_optionals_dataclass, only_name_optionals_dataclass = None, None, None, None, None, None, None
valid_json_cc, valid_json_sc, only_name_json = {}, {}, {}
from .classes import get_instance
locals().update(get_instance())

from http_server_base.model import DataClassJsonEncoder, EncoderError

class DataClassJsonEncoderTestCase(IEncoderBaseTestCase):
    encoder = DataClassJsonEncoder
    
    encode_single_params = \
    [
        # tp, [value], [expected], *other
        (InheritedJsonDataclass, inherited_json_dataclass_instance, valid_json_sc, dict()),
        (DecoratedJsonDataclass, decorated_json_dataclass_instance, valid_json_sc, dict()),
        (InheritedJsonDataclass, inherited_json_dataclass_instance, valid_json_sc, dict(camel_case_switch=False)),
        (DecoratedJsonDataclass, decorated_json_dataclass_instance, valid_json_sc, dict(camel_case_switch=False)),
        (InheritedJsonDataclass, inherited_json_dataclass_instance, valid_json_cc, dict(camel_case_switch=True)),
        (DecoratedJsonDataclass, decorated_json_dataclass_instance, valid_json_cc, dict(camel_case_switch=True)),
    ]
    
    def test_decode_invalid_class(self):
        """
        Test DataClassJsonEncoder.decode_single() on invalid model class.
        Check DataClassJsonEncoder raises a TypeError for model which is not a JSON Dataclasses 
        """
        for tp in [ None, int, 14, decorated_json_dataclass_instance, SimpleClass, SimpleClassWithInit, Dataclass ]:
            self.check_decode_raises_single(tp, valid_json_sc, TypeError)
    def test_decode_invalid_object(self):
        """
        Test DataClassJsonEncoder.decode_single() on invalid objects.
        Check DataClassJsonEncoder raises a EncoderError for objects those do not match model. 
        """
        
        params = \
        [
            (InheritedJsonDataclass, valid_json_cc, EncoderError, dict()),
            (DecoratedJsonDataclass, valid_json_cc, EncoderError, dict()),
            (InheritedJsonDataclass, valid_json_cc, EncoderError, dict(camel_case_switch=False)),
            (DecoratedJsonDataclass, valid_json_cc, EncoderError, dict(camel_case_switch=False)),
            (InheritedJsonDataclass, only_name_json, EncoderError, dict()),
            (DecoratedJsonDataclass, only_name_json, EncoderError, dict()),
        ]
        for model, value, error, kwargs in params:
            self.check_decode_raises_single(model, value, error, **kwargs)
    def test_decode_invalid_object_type(self):
        """
        Test DataClassJsonEncoder.decode_single() on invalid object types.
        Check DataClassJsonEncoder raises a EncoderError for objects those have invalid type. 
        """
        
        params = \
        [
            [ valid_json_cc ],
            "text",
            None,
            14,
        ]
        for arg in params:
            self.check_decode_raises_single(InheritedJsonDataclass, arg, EncoderError)

if (__name__ == '__main__'):
    from unittest import main as unittests_main
    unittests_main()
