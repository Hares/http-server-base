from copy import deepcopy
from typing import *

from http_server_base.tools.filters import *
from tests.helpers import TestCaseWithPrettyTestName

class FiltersTest(TestCaseWithPrettyTestName):
    
    @property
    def non_collections(self) -> List[Tuple[Any, Any]]:
        items = [ 'test', None, 1, 2, 3, True, False ]
        return [ (i, i) for i in items ]
    
    @property
    def lists(self) -> List[Tuple[List[Optional[Any]], List[Any]]]:
        return \
        [
            ([ None ], []),
            ([ 1, 2, None, 4 ], [ 1, 2, 4 ]),
            ([ 1, 2, 4 ], [ 1, 2, 4 ]),
            ([ '', True, False ], [ '', True, False ]),
        ]
    
    @property
    def dicts(self) -> List[Tuple[Dict[str, Optional[Any]], Dict[str, Any]]]:
        return \
        [
            (dict(a=None), dict()),
            (dict(a=1, b=2, c=None, d=4), dict(a=1, b=2, d=4)),
            (dict(a=1, b=2, d=4), dict(a=1, b=2, d=4)),
            (dict(a='', b=True, d=False), dict(a='', b=True, d=False)),
        ]
    
    @property
    def complex_structures(self) -> List[Tuple[Container[Optional[Any]], Container[Any]]]:
        lst = self.lists
        dct = self.dicts
        
        lst2 = deepcopy(lst)
        for i, d in enumerate(dct):
            lst2[i][0].append(d[0])
            lst2[i][1].append(d[1])
        
        dct2 = deepcopy(dct)
        for i, l in enumerate(lst):
            dct2[i][0]['e'] = l[0]
            dct2[i][1]['e'] = l[1]
        
        # noinspection PyTypeChecker
        return lst2 + dct2
    
    def test_filter_list(self):
        """ Test filters.list_filter_out() """
        
        cases: List[Tuple[str, List[Tuple[List[Optional[Any]], List[Any]]]]] = \
        [
            ("Simple case", self.lists),
            ("Complex case", self.complex_structures),
        ]
        for case, items in cases:
            for lst, expected in items:
                if (not isinstance(lst, list)):
                    continue
                
                with self.subTest(case, item=lst):
                    actual = list_filter_out(lst)
                    self.assertListEqual(actual, expected)
    
    def test_filter_dict(self):
        """ Test filters.dict_filter_out() """
        
        cases: List[Tuple[str, List[Tuple[Dict[str, Optional[Any]], Dict[str, Any]]]]] = \
        [
            ("Simple case", self.dicts),
            ("Complex case", self.complex_structures),
        ]
        for case, items in cases:
            for dct, expected in items:
                if (not isinstance(dct, dict)):
                    continue
                
                with self.subTest(case, item=dct):
                    actual = dict_filter_out(dct)
                    self.assertDictEqual(actual, expected)
    
    def test_filter_smart(self):
        """ Test filters.dict_filter_out() """
        
        cases: List[Tuple[str, List[Tuple[Any, Any]]]] = \
        [
            ("List", self.dicts),
            ("Dict", self.dicts),
            ("Complex structure", self.complex_structures),
            ("Non-collection", self.non_collections),
        ]
        for case, items in cases:
            for item, expected in items:
                with self.subTest(case, item=item):
                    actual = filter_out_smart(item)
                    self.assertEqual(actual, expected)

if (__name__ == '__main__'):
    from unittest import main as unittests_main
    unittests_main()
