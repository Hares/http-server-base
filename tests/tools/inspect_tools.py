from dataclasses import dataclass
from typing import *

from dataclasses_json import DataClassJsonMixin

from http_server_base.tools import is_list_type, unfold_list_type, unfold_json_dataclass_list_type
from tests.helpers import TestCaseWithPrettyTestName

class ListChild(List[str]):
    pass

T1 = TypeVar('T1')
T2 = TypeVar('T2')
class GenericListChild(List[str], Generic[T1, T2]):
    a: T1
    b: T2
    def __init__(self, a: T1, b: T2):
        super().__init__()
        self.a = a
        self.b = b

@dataclass
class CustomJsonDataclass(DataClassJsonMixin):
    name: str

class InspectToolsTestCase(TestCaseWithPrettyTestName):
    def test_is_list(self):
        """
        Test inspect_tools.is_list_type()
        """
        
        params: List[Tuple[str, type, bool]] = \
        [
            ("int",                         int,                         False),
            ("list",                        list,                        False),
            ("List",                        List,                        True),
            ("List[int]",                   List[int],                   True),
            ("ListChild",                   ListChild,                   True),
            ("List[ListChild]",             List[ListChild],             True),
            ("List[CustomJsonDataclass]",   List[CustomJsonDataclass],   True),
            ("CustomJsonDataclass",         CustomJsonDataclass,         False),
            ("GenericListChild",            GenericListChild,            True),
            ("GenericListChild[int, bool]", GenericListChild[int, bool], True),
        ]
        for name, tp, expected in params:
            with self.subTest(name, expected=expected):
                self.assertEqual(expected, is_list_type(tp))
    
    def test_unfold_list_type(self):
        """
        Test inspect_tools.unfold_list_type()
        """
        
        params: List[Tuple[str, type, Optional[type]]] = \
        [
            ("int",                         int,                         None),
            ("list",                        list,                        None),
            ("List",                        List,                        None),
            ("List[int]",                   List[int],                   int),
            ("ListChild",                   ListChild,                   None),
            ("List[ListChild]",             List[ListChild],             ListChild),
            ("List[CustomJsonDataclass]",   List[CustomJsonDataclass],   CustomJsonDataclass),
            ("CustomJsonDataclass",         CustomJsonDataclass,         None),
            ("GenericListChild",            GenericListChild,            None),
            ("GenericListChild[int, bool]", GenericListChild[int, bool], None),
        ]
        for name, tp, expected in params:
            with self.subTest(name, expected=expected):
                self.assertEqual(expected, unfold_list_type(tp))
    
    def test_unfold_json_dataclass_list_type__error(self):
        """
        Test inspect_tools.unfold_json_dataclass_list_type() raising exception for non-dataclass-json types
        """
        
        params: List[Tuple[str, type]] = \
        [
            ("int",                         int),
            ("list",                        list),
            ("List",                        List),
            ("List[int]",                   List[int]),
            ("ListChild",                   ListChild),
            ("List[ListChild[int]]",        List[ListChild]),
            # ("List[CustomJsonDataclass]",   List[CustomJsonDataclass]),
            # ("CustomJsonDataclass",         CustomJsonDataclass),
            ("GenericListChild",            GenericListChild),
            ("GenericListChild[int, bool]", GenericListChild[int, bool]),
        ]
        for name, tp in params:
            with self.subTest(name):
                self.assertRaises(TypeError, unfold_json_dataclass_list_type, tp)
    
    def test_unfold_json_dataclass_list_type__success(self):
        """
        Test inspect_tools.unfold_json_dataclass_list_type() raising exception for non-dataclass-json types
        """
        
        params: List[Tuple[str, type, Tuple[type, bool]]] = \
        [
            # ("int",                         int),
            # ("list",                        list),
            # ("List",                        List),
            # ("List[int]",                   List[int]),
            # ("ListChild",                   ListChild),
            # ("List[ListChild[int]]",        List[ListChild]),
            ("List[CustomJsonDataclass]",   List[CustomJsonDataclass], (CustomJsonDataclass, True)),
            ("CustomJsonDataclass",         CustomJsonDataclass,       (CustomJsonDataclass, False)),
            # ("GenericListChild",            GenericListChild),
            # ("GenericListChild[int, bool]", GenericListChild[int, bool]),
        ]
        for name, tp, expected in params:
            with self.subTest(name, expected=expected):
                self.assertEqual(expected, unfold_json_dataclass_list_type(tp))


if (__name__ == '__main__'):
    from unittest import main as unittests_main
    unittests_main()
