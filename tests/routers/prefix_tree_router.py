import re
from itertools import product
from typing import List, Union, Callable, Tuple, Pattern

from dataclasses import dataclass, field

from http_server_base import IApplication, HandlerListType
from http_server_base.prefix_tree_router import PrefixTreeRouter
from tests.helpers import TestCaseWithPrettyTestName

@dataclass(init=False)
class ApplicationProxy(IApplication):
    logger_name: str = 'test'
    handlers: HandlerListType = field(default_factory=list)
    # noinspection PyMissingConstructor
    def __init__(self):
        self.handlers = list()

class PrefixTreeRouterTests(TestCaseWithPrettyTestName):
    
    app_proxy: ApplicationProxy
    router: PrefixTreeRouter
    
    @classmethod
    def setUpClass(cls):
        cls.app_proxy = ApplicationProxy()
        # noinspection PyTypeChecker
        cls.router = PrefixTreeRouter(cls.app_proxy)
    
    def check_unescape(self, pattern: Union[str, Pattern[str], bytes], expected_key: str, expected_match: str = None, *, respect_none: bool = True):
        with self.subTest(f"Test unescape '{pattern}'", pattern=pattern, expected_key=expected_key, expected_match=expected_match):
            print(f"Testing '{pattern}' => '{expected_key}' (match arg: '{expected_match}')...")
            actual_key, actual_match = self.router.unescape(pattern)
            print(f"Got result: '{actual_key}' (match arg: '{actual_match}')")
            
            if (respect_none):
                if (expected_match is None):
                    expected_match = expected_key
                if (actual_match is None):
                    actual_match = actual_key
            
            self.assertEqual(expected_key, actual_key)
            self.assertEqual(expected_match, actual_match)
    
    def test_unescape_regexp(self):
        """
        Test PrefixTreeRouter.unescape() on regexp
        
        Checks how PrefixTreeRouter.unescape() works on regexp:
         - removes brackets,
         - removes trailing wildcards, /'s, and $'s,
         - removes leading ^'s
         - unescapes special characters
         - respects both str and compiled regexp
        """
        
        encode_options: List[List[Union[None, str, Callable[[str], str]]]] = \
        [
            [ None, r'{}/' ],
            [ None, lambda p: re.escape(p), ],
            [ None, r'^{}', ],
            [ None, r'{}/', r'{}/?', r'{}//' ],
            [ None, r'{}.*', r'{}(.*)', r'{}(|/.*)', r'{}(|(/.*))' ],
            [ None, r'{}$', ],
            [ None, lambda p: p.encode(), lambda p: re.compile(p) ],
        ]
        
        example = '/robots.txt'
        
        data: List[Tuple[Union[str, Pattern[str]], Tuple[str, str]]] = list()
        for comb in product(*encode_options):
            pattern = example
            for func in comb: # type: Union[None, str, Callable[[str], str]]
                if (func is None):
                    pass
                elif (isinstance(func, str)):
                    pattern = func.format(pattern)
                else:
                    pattern = func(pattern)
            
            tup = pattern, (example, example)
            data.append(tup)
        
        for pattern, (expected_key, expected_match) in data:
            self.check_unescape(pattern, expected_key, expected_match)
    
    def test_unescape_regexp_match_groups(self):
        """
        Test PrefixTreeRouter.unescape() on regexp with brackets
        
        PrefixTreeRouter.unescape() should remove brackets and
        consider all before them as a fist match arg.
        """
        
        example = '/robots.txt'
        
        data: List[Tuple[Union[str, Pattern[str]], Tuple[str, str]]] = list()
        for prefix in [ '', '/files' ]:
            data.append((prefix + '/(robots.txt)$', (prefix + example, prefix)))
            data.append((prefix + '/((robots).txt)$', (prefix + example, prefix)))
            data.append((prefix + '/(robots.(txt))$', (prefix + example, prefix)))
        
        for pattern, (expected_key, expected_match) in data:
            self.check_unescape(pattern, expected_key, expected_match)

if (__name__ == '__main__'):
    from unittest import main
    main()
