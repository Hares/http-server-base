#!/usr/bin/env bash
set -e

if [[ -z ${PYTHON} ]]
then
    PYTHON=python3.7
    echo Counting PYTHON as ${PYTHON}
fi
if [[ -z ${NAME} ]]
then
    NAME=http-server-base
    echo Counting NAME as ${NAME}
fi

rm -rf dist/${NAME}-*.tar.gz
${PYTHON} setup.py sdist
twine upload dist/${NAME}-*.tar.gz

echo Success!
